
from flask import Flask, render_template, request, redirect, url_for
import psycopg2

app = Flask(__name__)

# PostgreSQL connection configuration
try:
    conn = psycopg2.connect(
        dbname="koyebdb",
        user="koyeb-adm",
        password="oY4BfRikNV9G",
        host="ep-old-sun-a2ldyn5h.eu-central-1.pg.koyeb.app"
    )
    print("Connected to PostgreSQL successfully!")
except psycopg2.Error as e:
    print("Error: Could not connect to PostgreSQL")
    print(e)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/submit', methods=['POST'])
def submit():
    if request.method == 'POST':
        # Get form data
        name = request.form['name']
        additional_info = request.form['additional_info']
        email = request.form['email']

        # Store data in the database
        cursor = conn.cursor()
        cursor.execute("INSERT INTO users (name, additional_info, email) VALUES (%s, %s, %s)",
                       (name, additional_info, email))
        conn.commit()
        cursor.close()

        # Redirect to a "Thank You" page where the links are displayed
        return redirect(url_for('thank_you'))

@app.route('/thank-you')
def thank_you():
    return render_template('thank_you.html')

if __name__ == '__main__':
    app.run(debug=True)