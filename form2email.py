# Flask app
from flask import Flask, request
import psycopg2

app = Flask(__name__)

# PostgreSQL connection configuration
conn = psycopg2.connect(
    dbname="your_database_name",
    user="your_database_user",
    password="your_database_password",
    host="localhost"
)

@app.route('/subscribe', methods=['POST'])
def subscribe():
    email = request.form.get('email')
    if email:
        cursor = conn.cursor()
        cursor.execute("INSERT INTO subscribers (email, status) VALUES (%s, %s)", (email, 'subscribed'))
        conn.commit()
        cursor.close()
        return 'Subscribed successfully'
    else:
        return 'Invalid email', 400

if __name__ == '__main__':
    app.run()

# Cron job script
import psycopg2
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

# Connect to the database
conn = psycopg2.connect(
    dbname="your_database_name",
    user="your_database_user",
    password="your_database_password",
    host="localhost"
)

def send_newsletter():
    cursor = conn.cursor()
    cursor.execute("SELECT email FROM subscribers WHERE status = 'subscribed'")
    subscribers = cursor.fetchall()
    cursor.close()

    for subscriber in subscribers:
        email = subscriber[0]
        send_email(email, "Your Newsletter Subject", "Your Newsletter Content")

def send_email(to_email, subject, body):
    # Configure SMTP server
    smtp_server = 'smtp.example.com'
    smtp_port = 587
    sender_email = 'your_email@example.com'
    sender_password = 'your_email_password'

    # Create message
    msg = MIMEMultipart()
    msg['From'] = sender_email
    msg['To'] = to_email
    msg['Subject'] = subject
    msg.attach(MIMEText(body, 'plain'))

    # Send email
    with smtplib.SMTP(smtp_server, smtp_port) as server:
        server.starttls()
        server.login(sender_email, sender_password)
        server.send_message(msg)

if __name__ == '__main__':
    send_newsletter()

# Cron job setup (run this command to edit crontab)
# crontab -e

# Example cron job configuration (runs the script every day at 6 AM)
# 0 6 * * * /path/to/python /path/to/cron_job_script.py
